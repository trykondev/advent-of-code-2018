#Advent of Code Day 14 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

scoreboard = []
scoreboard.append(3)
scoreboard.append(7)

#each elf will have a current index, the score of that index?
elf_one = (0, scoreboard[0])
elf_two = (1, scoreboard[1])


def print_scoreboard():
    global scoreboard
    global elf_one
    global elf_two
    final_string = ''
    for index in xrange(0, len(scoreboard)):
        thing_to_add = str(scoreboard[index])
        if index == elf_one[0]:
            thing_to_add = '('+ thing_to_add + ')';
        elif index == elf_two[0]:
            thing_to_add = '['+ thing_to_add + ']';
        final_string = final_string + thing_to_add + ' '
    print final_string

def update_scoreboard():
    global scoreboard
    global elf_one
    global elf_two
    sum = elf_one[1] + elf_two[1]
    str_sum = str(sum)
    for c in str_sum:
        scoreboard.append(int(c))
    elf_one_offset = elf_one[1] + 1
    elf_two_offset = elf_two[1] + 1

    for x in xrange(0, elf_one_offset):
        elf_one = (elf_one[0] + 1, elf_one[1])
        if elf_one[0] == len(scoreboard):
             elf_one = (0, elf_one[1])
    elf_one = (elf_one[0], scoreboard[elf_one[0]])

    for x in xrange(0, elf_two_offset):
        elf_two = (elf_two[0] + 1, elf_two[1])
        if elf_two[0] == len(scoreboard):
             elf_two = (0, elf_two[1])
    elf_two = (elf_two[0], scoreboard[elf_two[0]])

recipies = 147061
recipie_counter = 0

print_scoreboard()
while recipie_counter < recipies + 11:
    update_scoreboard()
    #print_scoreboard()
    recipie_counter = recipie_counter + 1

final_output = ''
for n in xrange(recipies, recipies + 10):
    final_output = final_output + str(scoreboard[n])

print 'The next ten recipies will be : ' + final_output
