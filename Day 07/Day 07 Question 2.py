#Advent of Code Day 7 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

'''
s = 1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
'''

s = '''Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.'''

s = open(filename, 'r').read()
s = s.split('\n')

#Build a map of successors
successor_map = {}
prereq_map = {}
unique_letters = set()
time_step = 0
worker_map = {}
worker_total = 5
for i in range(0, worker_total):
    worker_map[i] = '.'

letter_time_map = {}
for z in xrange(0, 26):
    #For real version:
    letter_time_map[chr(z + 65)] = 61+z
    #letter_time_map[chr(z + 65)] = 1 +z
for k in sorted(letter_time_map.keys()):
    print letter_time_map[k]

for entry in s:
    #print entry
    arr = entry.split(' ')
    pre_letter = arr[1]
    post_letter = arr[7]
    unique_letters.add(pre_letter)
    unique_letters.add(post_letter)

    if pre_letter not in successor_map:
        successor_map[pre_letter] = [post_letter]
    else:
        successor_map[pre_letter].append(post_letter)

    if post_letter not in prereq_map:
        prereq_map[post_letter] = [pre_letter]
    else:
        prereq_map[post_letter].append(pre_letter)


'''
print 'List of successors: '
print successor_map

print '\nList of prereqs: '
print prereq_map

print '\nList of unique letters: '
print unique_letters
'''

starting_letter_list = []

for letter in unique_letters:
    if letter not in prereq_map:
        starting_letter_list.append(letter)

starting_letter = starting_letter_list[0]
print 'Starting letters: '
print starting_letter_list

print 'Starting letter: ' + starting_letter
stack = []


result_string = ''
completed_letters = []

def print_worker_table():
    global time_step
    global worker_map
    global completed_letters
    
    str_entry = ''
    str_entry = str_entry + '   ' + str(time_step)

    for key in worker_map:
        str_entry = str_entry + '\t\t  ' + worker_map[key]

    str_entry = str_entry + '\t\t'
    for val in completed_letters:
        str_entry = str_entry + val

    print str_entry

def ready_to_process(let, sll, p_m, c_l):

    if let in sll:
        return True
    result = True
    
    for item in p_m[let]:
        if item not in c_l:
            result = False

    return result


print '\n\nALGORITHM: '
for le in starting_letter_list:
    stack = sorted(starting_letter_list, reverse=True)

print 'OFFICIALLY STARTING'
str_out = ''
str_out = str_out + 'Second\t'
for n in xrange(0, worker_total):
    str_out = str_out + '\t' + 'Worker ' + str(n+1)
str_out = str_out + '\tDone'
print str_out

def work_on_letter(letter):
    global letter_time_map
    global completed_letters


    print 'working on ' + letter + ' and letter count: ' + str(letter_time_map[letter])
    letter_time_map[letter] = letter_time_map[letter] - 1
    if letter_time_map[letter] < 0:
        letter_time_map[letter] = 0
    if letter_time_map[letter] == 0:
        return True
    else:
        return False

def unassign_workers(letter):
    global worker_map
    for key in worker_map:
        if worker_map[key] == letter:
            worker_map[key] = '.'

failsafe = 0
while len(stack) > 0:
    
    
    stack = sorted(stack, reverse=True)
    next_list = []
    ready_list = []
    next_list = sorted(stack)
    print next_list
    for entry in worker_map:
        if worker_map[entry] in next_list:
            next_list.remove(worker_map[entry])
    #print 'Assigning workers:'

    ready_list = []
    for entry in next_list:
        if ready_to_process(entry, starting_letter_list, prereq_map, completed_letters):
            ready_list.append(entry)


    for worker in worker_map:
       # print next_list
        if worker_map[worker] == '.' and len(ready_list) > 0:
                worker_map[worker] = ready_list[0]
                del ready_list[0]
    #next = stack.pop()

    print_worker_table()
    for worker in worker_map:
         if worker_map[worker] != '.':
            if ready_to_process(worker_map[worker], starting_letter_list, prereq_map, completed_letters):
        
        
        
                res = work_on_letter(worker_map[worker])
        
                if(res):
                   # stack.pop()
                    stack.remove(worker_map[worker])
                    if worker_map[worker] not in completed_letters:
                        completed_letters.append(worker_map[worker])
                
                    if worker_map[worker] not in result_string:
                        result_string = result_string + worker_map[worker]
                    if worker_map[worker] in successor_map:
                        succ = sorted(successor_map[worker_map[worker]], reverse=True)
                       # print 'succ:'
                       # print succ
                        for item in succ:
                            if (item not in completed_letters) and (item not in stack):
                                stack.append(item)
                        #del next_list[0]
                    unassign_workers(worker_map[worker])
        
            else:
                if worker_map[worker] not in completed_letters:
                    stack.append(worker_map[worker])
                #print 'appending: ' + next


    time_step = time_step + 1
    failsafe = failsafe + 1


print 'Completed letters: '
print completed_letters


print 'Resulting order: ' + result_string
print 'Resulting time count: ' + str(time_step)



'''
CHILFNMORYKGAQXUVBZPSJWDET








  -->A--->B----->G
 /    \      \
C      -->D----->E
 \           /
  ---->F-----
  
'H', 'C', 'R', 'L', 'F'])

our boi failed the test: E
our boi failed the test: J
our boi failed the test: W
our boi failed the test: X
our boi failed the test: Z
our boi failed the test: X
our boi failed the test: Q
our boi failed the test: S
our boi failed the test: T


CIHNLFYMROKGAQXUZPVBSJWDET

'''




