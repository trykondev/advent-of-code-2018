#Advent of Code Day 7 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

'''
s = 1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
'''

s = '''Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step B must be finished before step G can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.'''

s = open(filename, 'r').read()
s = s.split('\n')

#Build a map of successors
successor_map = {}
prereq_map = {}
unique_letters = set()

for entry in s:
    print entry
    arr = entry.split(' ')
    pre_letter = arr[1]
    post_letter = arr[7]
    unique_letters.add(pre_letter)
    unique_letters.add(post_letter)

    if pre_letter not in successor_map:
        successor_map[pre_letter] = [post_letter]
    else:
        successor_map[pre_letter].append(post_letter)

    if post_letter not in prereq_map:
        prereq_map[post_letter] = [pre_letter]
    else:
        prereq_map[post_letter].append(pre_letter)



print 'List of successors: '
print successor_map

print '\nList of prereqs: '
print prereq_map

print '\nList of unique letters: '
print unique_letters

starting_letter_list = []

for letter in unique_letters:
    if letter not in prereq_map:
        starting_letter_list.append(letter)
        #break
starting_letter = starting_letter_list[0]
print 'Starting letters: '
print starting_letter_list
#Create stack to track order of steps to be processed
print 'Starting letter: ' + starting_letter
stack = []
#stack.append(starting_letter)

result_string = ''
completed_letters = set()

def ready_to_process(let, sll, p_m, c_l):

    if let in sll:
        return True
    result = True
    
    for item in p_m[let]:
        if item not in c_l:
            result = False

    return result


print '\n\nALGORITHM: '
for le in starting_letter_list:
    #completed_letters.add(le)
    stack = sorted(starting_letter_list, reverse=True)

print 'starting stack: '
print stack
print 'OFFICIALLY STARTING'
while len(stack) > 0:
    stack = sorted(stack, reverse=True)
    next = stack.pop()
    if ready_to_process(next, starting_letter_list, prereq_map, completed_letters):
        print 'processing ' + next
        completed_letters.add(next)
        result_string = result_string + next
        if next in successor_map:
            succ = sorted(successor_map[next], reverse=True)
            print succ
            for item in succ:
                print 'WHAT'
                print completed_letters
                print stack
                if (item not in completed_letters) and (item not in stack):
                #if True:
                    print 'adding letter to stack: ' + item
                    stack.append(item)
                else:
                    print 'already IN the completed letters: ' + item

    else:
        #stack.insert(0, next)
        print 'our boi failed the test: ' + next
        continue



print 'Completed letters: '
print completed_letters


print 'Resulting order: ' + result_string
print sorted(result_string)

#Stack Sample:
#Completed: C A B
# F D E


#TRIED:   CHMLFKGAORQXUVBZPWDSJET
#TRIED2:  CHMLFKGAORQXUVBZPWDSJETINY
#TRIED3:  CHLFRIONMYKGAQXUZPSJVBWDET
#TRIED4:  CHLFRIONMYKGAQXUZPSJVBWDET


'''

  -->A--->B----->G
 /    \      \
C      -->D----->E
 \           /
  ---->F-----
  
'H', 'C', 'R', 'L', 'F'])
  
our boi failed the test: E
our boi failed the test: J
our boi failed the test: W
our boi failed the test: X
our boi failed the test: Z
our boi failed the test: X
our boi failed the test: Q
our boi failed the test: S
our boi failed the test: T
  
'''




