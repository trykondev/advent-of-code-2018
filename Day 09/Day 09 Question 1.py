#Advent of Code Day 9 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()


s = s.split(' ')
player_count = int(s[0])
last_marble_total = int(s[6])

print 'Player count: ' + str(player_count)
print 'Last marble total ' + str(last_marble_total)

marble_circle = []
current_marble_index = 0

def change_position(amount):
    global current_marble_index
    global marble_circle
    
    if amount >= 0:
        for amt in xrange(0, amount):
            current_marble_index = current_marble_index + 1
            if current_marble_index == len(marble_circle):
                current_marble_index = 0
    else:
        amount = amount * -1
        for amt in xrange(0, amount):
            current_marble_index = current_marble_index - 1
            if current_marble_index == -1:
                current_marble_index = len(marble_circle) - 1

def print_marble_circle():
    return
    global current_marble_index
    global marble_circle
    global player_index
    
    str_out = ''
    for i in xrange(0, len(marble_circle)):
        if i == current_marble_index:
            str_out = str_out + '(' + str(marble_circle[i]) + ') '
        else:
            str_out = str_out + str(marble_circle[i]) + ' '
    print '[' + str(player_index) + '] ' + str_out

#player_count = 30
#last_marble_total = 5807
player_map = {}

for n in xrange(0, player_count):
    player_map[n+1] = 0

player_index = 1
marble_circle.append(0)
print_marble_circle()
marble_circle.append(1)
current_marble_index = 1
print_marble_circle()
current_marble_index = 1
player_index = 2

for x in xrange(2, last_marble_total+1):
    if x % 23 == 0:
        player_map[player_index] = player_map[player_index] + x
        change_position(-7)
        print 'Removing marble: ' + str(marble_circle[current_marble_index])
        player_map[player_index] = player_map[player_index] + marble_circle[current_marble_index]
        del marble_circle[current_marble_index]
        print_marble_circle()
    else:
        change_position(2)
        if(current_marble_index == 0):
            marble_circle.append(x)
            current_marble_index = len(marble_circle)-1
        else:
            marble_circle.insert(current_marble_index, x)
        print_marble_circle()
    player_index = player_index +1
    if player_index > player_count:
        player_index = 1


final_scores = sorted(player_map.values(), reverse=True)
print 'Player Map'
print player_map
print 'Winning score: ' + str(final_scores[0])






