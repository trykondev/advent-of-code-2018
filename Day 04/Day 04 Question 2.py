#Advent of Code Day 4 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()


s = '''[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up'''

s = open(filename, 'r').read()
s_arr = s.split('\n')


schedule_hash = {}

for entry in s_arr:
  
  text_arr = entry.split(']')
  text_arr[1] = text_arr[1][1:]
  
  entry = entry.split(' ')
  date_arr = entry[0].split('-')
  mins_arr = entry[1].split(':')
  aggregate = []
  aggregate.append(date_arr[0])
  aggregate.append(date_arr[1])
  aggregate.append(date_arr[2])
  aggregate.append(mins_arr[0])
  aggregate.append(mins_arr[1][0:len(mins_arr[1])-1])
  
  tup = (date_arr[0], date_arr[1], date_arr[2], mins_arr[0], mins_arr[1][0:len(mins_arr[1])-1])
  schedule_hash[tup] = text_arr[1]

#for key in unsorted_hash:
#  print key, '->', unsorted_hash[key]
  
#sorted_hash = sorted(unsorted_hash,key=lambda k: unsorted_hash[1])
key_list = sorted(schedule_hash)


big_date_list = []
date_list = []
text_list = []
big_guard_list = []
guard_list = []

for key in key_list:
  print key, '->', schedule_hash[key]
  big_date_list.append((key[1], key[2]))
  text_list.append(schedule_hash[key])

for date in big_date_list:
  if(date not in date_list):
    date_list.append(date)

  
for text in text_list:
  #print text
  if 'Guard' in text:
    big_guard_list.append(text)
  

date_list.append(('02', '19'))
date_list.append(('03', '17'))
date_list.append(('05', '03'))

date_list = sorted(date_list)

for ga in big_guard_list:
  ar = ga.split(' ')
  guard_list.append(ar[1][1:])

print 'Number of dates: ' + str(len(date_list))
print 'Number of guards: ' + str(len(guard_list))

print '\n\n'




list_of_lists = []
curr_list = []
for key in key_list:
  print key, schedule_hash[key]
  if('Guard' in schedule_hash[key]):
    list_of_lists.append(curr_list)
    curr_list = []
  else:
    curr_list.append((key[4], schedule_hash[key]))
del list_of_lists[0]
list_of_lists.append(curr_list)

for entry in list_of_lists:
  print entry

guard_sleep_hash = {}

for num in xrange(0, len(guard_list)):
  print 'RUN'
  mode = 0
  sleep_str = ''
  if(len(list_of_lists) > 0):
    list_entry = list_of_lists[0]
  else:
    list_entry = []
  for i in xrange(0, 60):
    #print 'num ' + str(num)
    #print 'len ' + str(len(list_of_lists))
    if len(list_of_lists) > 0:
      list_entry = list_of_lists[0]
      if(len(list_entry) > 0):
        if int(list_entry[0][0]) == i:
          if 'wakes up' in list_entry[0][1]:
            print 'WOKE' + str(guard_list[num])
            mode = 0
          else:
            print 'NOT WOKE' + str(guard_list[num])
            mode = 1
          del list_entry[0]
    
    if mode == 0:
      sleep_str = sleep_str + '.'
    else: 
      sleep_str = sleep_str + '#'
  if(len(list_of_lists) > 0):
    print 'didja'
    del list_of_lists[0]
  guard_sleep_hash[(date_list[num], guard_list[num])] = sleep_str

for g in guard_sleep_hash:
  print g, guard_sleep_hash[g]






total_minutes_hash = {}

print '\n\nSchedule: \n'

min_tens_string = ''
min_ones_string = ''

for i in xrange(0, 60):
  min_tens_string = min_tens_string + str(i/10)
  min_ones_string = min_ones_string + str(i%10)

print min_tens_string
print min_ones_string

print 'Date\tID\tMinute'
print '\t\t' + min_tens_string
print '\t\t' + min_ones_string

for i in xrange(0, len(date_list)):

  sleep_string = guard_sleep_hash[(date_list[i], guard_list[i])]
  print str(date_list[i][0]) + '-' + str(date_list[i][1]) + '\t' + '#' + str(guard_list[i]) + '\t' + sleep_string
  if guard_list[i] not in total_minutes_hash:
    total_minutes_hash[guard_list[i]] = 0
  for char in sleep_string:
    if char == '#':
       total_minutes_hash[guard_list[i]] =  total_minutes_hash[guard_list[i]] + 1
        
print '\nTotal Minutes Hash:'
print total_minutes_hash

max_id = -1
max_mins = -1

for key in total_minutes_hash:
  if total_minutes_hash[key] > max_mins:
    max_mins = total_minutes_hash[key]
    max_id = key
    


frequency_hash = {}


list_final = []
super_hash = {}

for i in xrange(0, len(date_list)):
  if guard_list[i] not in super_hash:
    super_hash[guard_list[i]] = {}
    for z in xrange(0, 60):
       super_hash[guard_list[i]][z] = 0

  sleep_string = guard_sleep_hash[(date_list[i], guard_list[i])]
  for n in xrange(0, len(sleep_string)):
    if sleep_string[n] == '#':
        super_hash[guard_list[i]][n] =  super_hash[guard_list[i]][n] + 1

  max_freq = -1
  max_freq_min = -1
  
  for key in  super_hash[guard_list[i]]:
    if  super_hash[guard_list[i]][key] > max_freq:
      max_freq =  super_hash[guard_list[i]][key]
      max_freq_min = key

  list_final.append((guard_list[i], max_freq, max_freq_min))

final_freq = 0;
final_min = 0;
final_id = 0;

list_extra = list_final
print '\n\n\nTEST'
print sorted(list_extra)

for entry in list_final:
  if int(entry[1]) > final_freq:
    final_freq = int(entry[1])
    print 'data: ' + str(entry[0]) + ' '  + str(entry[2])
    final_min = int(entry[2])
    final_id = int(entry[0])

print '\nFinal answer: ' + str(int(final_id) * int(final_min))
        

  
