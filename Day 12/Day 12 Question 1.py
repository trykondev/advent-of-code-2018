#Advent of Code Day 12 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

initial_state = s[0].split(' ')[2]

print 'initial state: '
#print initial_state
#print len(initial_state)

state_map = {}
rules_map = {}
max_index = 0
min_index = 0

print 'Rule parts: '
for m in xrange(2, len(s)):
    rule_parts = s[m].split(' => ')
    print rule_parts
    rules_map[rule_parts[0]] = rule_parts[1]

for i in xrange(0, len(initial_state)):
    state_map[i] = initial_state[i]

print rules_map

def print_current_state():
    global state_map
    global max_index
    global min_index
    
    max_index = 0
    min_index = 0
    
    output_string = ''
    
    for entry in state_map:
        if entry < min_index:
            min_index = entry
        if entry > max_index:
            max_index = entry

    for j in xrange(min_index - 35, max_index + 35):
        if j in state_map:
            output_string = output_string + state_map[j]
        else:
            output_string = output_string + '.'
            state_map[j] = '.'
    print output_string

def get_five_segment(index):
    global state_map
    segment = ''
    for n in xrange(index-2, index + 3):
        if n in state_map:
            segment = segment + state_map[n]
        else:
            segment = segment + '.'
    return segment
def create_new_map():
    global state_map
    global min_index
    global max_index
    next_generation_map = {}
    for i in xrange(min_index-5, max_index+6):
        five = get_five_segment(i)
        if five in rules_map:
            next_generation_map[i] = rules_map[five]
        else:
            next_generation_map[i] = state_map[i]
    state_map = next_generation_map
    '''
    for entry in state_map:
        if entry < min_index:
            min_index = entry
        if entry > max_index:
            max_index = entry
    '''

generation_count = 20
print_current_state()
for gen in xrange(0, generation_count):
    create_new_map()
    print_current_state()

plant_count = 0
for pcount in xrange(min_index - 5, max_index + 5):
    if pcount in state_map:
        if state_map[pcount] == '#':
            plant_count = plant_count + pcount
print 'Plant count: ' + str(plant_count)






















