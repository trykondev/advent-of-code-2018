#Advent of Code Day 12 Question 2

import sys
import os
import time

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

initial_state = s[0].split(' ')[2]

print 'initial state: '
#print initial_state
#print len(initial_state)

state_map = {}
rules_map = {}
max_index = 0
min_index = 0
next_five = ''
print 'Rule parts: '
for m in xrange(2, len(s)):
    rule_parts = s[m].split(' => ')
    print rule_parts
    rules_map[rule_parts[0]] = rule_parts[1]

for i in xrange(-30, len(initial_state)+30):
    if i >= 0 and i < len(initial_state):
        state_map[i] = initial_state[i]
        #print 'found er! at index ' + str(i)
    else:
        state_map[i] = '.'
    #print state_map
print rules_map

def print_current_state():
    global state_map
    global max_index
    global min_index
    
    max_index = 0
    min_index = 0
    
    output_string = ''
    
    for entry in state_map:
        if entry < min_index and state_map[entry] == '#':
            min_index = entry
        if entry > max_index and state_map[entry] == '#':
            max_index = entry

    for j in xrange(min_index - 5, max_index + 5):
        #print 'min and max: ' + str(min_index) + ' ' + str(max_index)
        if j in state_map:
            output_string = output_string + state_map[j]
        else:
            output_string = output_string + '.'
            state_map[j] = '.'
    #print output_string

def get_five_segment(index):
    start_time_5 = time.time()
    global state_map
    segment = ''
    for n in xrange(index-2, index + 3):
        if n in state_map:
            segment = segment + state_map[n]
        else:
            segment = segment + '.'
    end_time_5 = time.time()
    #print 'five seg: ' + str(end_time_5 - start_time_5)
    return segment

def get_first_rolling_five():
    global state_map
    global min_index
    global max_index
    rolling_five = ''
    rolling_five = rolling_five + state_map[min_index-5]
    rolling_five = rolling_five + state_map[min_index-4]
    rolling_five = rolling_five + state_map[min_index-3]
    rolling_five = rolling_five + state_map[min_index-2]
    rolling_five = rolling_five + state_map[min_index-1]
    print 'rolling five ' + rolling_five
    return rolling_five

def create_new_map():
    global next_five
    global state_map
    global min_index
    global max_index
    start_time = time.time()
    next_generation_map = {}



    for i in xrange(min_index-3, max_index+5):
        #print len(state_map)
        #print 'len of state map'
        if next_five in rules_map:
            next_generation_map[i] = rules_map[next_five]
        else:
            next_generation_map[i] = state_map[i]
        next_five = next_five[1:]
        if (i+3 in state_map):
            next_five = next_five + state_map[i+3]
        else:
            next_five = next_five + '.'

    state_map = next_generation_map
    end_time = time.time()
    #print 'Time: ' + str(end_time - start_time)
   
    
    
    #idea: instead of iterating over this five times each entry, just look at one and then append it to a rolling five
    '''
    for entry in state_map:
        if entry < min_index:
            min_index = entry
        if entry > max_index:
            max_index = entry
    '''
    end_time = time.time()
    #print 'Create new map time: ' + str(end_time - start_time)

#generation_count = 50000000000
generation_count = 2000
next_five = get_first_rolling_five()
print_current_state()
for gen in xrange(0, generation_count):
    create_new_map()
    print_current_state()
    if gen % 10000 == 0:
        print gen
    plant_count = 0
    for pcount in xrange(min_index - 5, max_index + 5):
        if pcount in state_map:
            if state_map[pcount] == '#':
                plant_count = plant_count + pcount
    print str(plant_count)
























