#Advent of Code Day 16 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n\n')

registers = [0,0,0,0]
before_stack = []
after_stack = []
instruction_stack = []

def print_regsiters():
    global registers
    str_reg = ''
    str_reg = str_reg + '[0]: ' + str(registers[0]) + '  '
    str_reg = str_reg + '[1]: ' + str(registers[1]) + '  '
    str_reg = str_reg + '[2]: ' + str(registers[2]) + '  '
    str_reg = str_reg + '[3]: ' + str(registers[3]) + '  '
    print str_reg

for entry in s:
    three_args = entry.split('\n')
    before = three_args[0].split(', ')
    before[0] = before[0].split('[')[1]
    before[3] = before[3].split(']')[0]
    after = three_args[2].split(', ')
    after[0] = after[0].split('[')[1]
    after[3] = after[3].split(']')[0]
    instruction = three_args[1].split(' ')

#    print 'Before'
#    print before
#    print 'Instruction'
#    print instruction
#    print 'After'
#    print after
#    print '@'

    before_stack.append(before)
    instruction_stack.append(instruction)
    after_stack.append(after)
    


#Each instruction has four values:

# OPCODE INPUT_A INPUT_B OUTPUT_C
# OPCODE -- behavior
# value A == immediate A == literally A
# register A -- use register number
# C -- Always a register

#Four Registers:
#[0]
#[1]
#[2]
#[3]

def set_registers(zero, one, two, three):
    global registers
    registers[0] = int(zero)
    registers[1] = int(one)
    registers[2] = int(two)
    registers[3] = int(three)


def test_opcode(code_id, inputA, inputB, outputC):
    global registers
    if code_id == 0:
        print 'Testing opcode add register'
        #Add two registers together
        registers[int(outputC)] = registers[int(inputA)] + registers[int(inputB)]
    elif code_id == 1:
        print 'Testing opcode add immediate'
        #Add register with literal value
        registers[outputC] = registers[inputA] + int(inputB)
    elif code_id == 2:
        print 'Testing opcode multiply register'
        #Multiply two registers together
        registers[int(outputC)] = registers[(inputA)] * registers[(inputB)]
    elif code_id == 3:
        print 'Testing opcode multiply immediate'
        #Multiply register with literal value
        registers[outputC] = registers[inputA] * int(inputB)
    elif code_id == 4:
        print 'Testing opcode bitwise AND register'
        registers[int(outputC)] = registers[(inputA)] & registers[(inputB)]
    elif code_id == 5:
        print 'Testing opcode bitwise AND immediate'
        registers[outputC] = registers[inputA] & int(inputB)
    elif code_id == 6:
        print 'Testing opcode bitwise OR register'
        registers[int(outputC)] = registers[(inputA)] | registers[(inputB)]
    elif code_id == 7:
        print 'Testing opcode bitwise OR immediate'
        registers[outputC] = registers[inputA] | int(inputB)
    elif code_id == 8:
        print 'Testing opcode set register'
        registers[int(outputC)] = registers[(inputA)]
    elif code_id == 9:
        print 'Testing opcode set immediate'
        registers[int(outputC)] = int(inputA)
    elif code_id == 10:
        print 'Testing opcode greater than immediate register'
        if inputA > registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
    elif code_id == 11:
        print 'Testing opcode greater than register immediate'
        if registers[inputA] > inputB:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
    elif code_id == 12:
        print 'Testing opcode greater than register register'
        if registers[inputA] > registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
    elif code_id == 13:
        print 'Testing opcode equality immediate register'
        if inputA == registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
    elif code_id == 14:
        print 'Testing opcode equality register immediate'
        if registers[inputA] == inputB:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
    elif code_id == 15:
        print 'Testing opcode equality register register'
        if registers[inputA] == registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0


def test_equality(a, b, c, d):
    global registers
    if registers[0] == int(a) and registers[1] == int(b) and registers[2] == int(c) and registers[3] == int(d):
        return True
    else:
        return False

print 'Test a few out:'

master_count = 0
for i in xrange(0, len(before_stack)):
    before_temp = before_stack[i]
    instruction_temp = instruction_stack[i]
    afterward_temp = after_stack[i]
    ticker = 0
    for n in xrange(0, 16):
        
        set_registers(before_temp[0], before_temp[1], before_temp[2], before_temp[3])
        #print 'Before: '
        #print_regsiters()
        #print 'Executing instruction: '
       # print instruction_temp
        test_opcode(n, int(instruction_temp[1]), int(instruction_temp[2]), int(instruction_temp[3]))
        if test_equality(afterward_temp[0], afterward_temp[1], afterward_temp[2], afterward_temp[3]):
            ticker = ticker + 1

    print 'Ticker count: ' + str(ticker)



print 'Final number of 3+ opcode instructions: ' + str(master_count)

