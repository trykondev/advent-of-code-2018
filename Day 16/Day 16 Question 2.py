#Advent of Code Day 16 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n\n')

registers = [0,0,0,0]
before_stack = []
after_stack = []
instruction_stack = []

def print_regsiters():
    global registers
    str_reg = ''
    str_reg = str_reg + '[0]: ' + str(registers[0]) + '  '
    str_reg = str_reg + '[1]: ' + str(registers[1]) + '  '
    str_reg = str_reg + '[2]: ' + str(registers[2]) + '  '
    str_reg = str_reg + '[3]: ' + str(registers[3]) + '  '
    print str_reg

for entry in s:
    three_args = entry.split('\n')
    before = three_args[0].split(', ')
    before[0] = before[0].split('[')[1]
    before[3] = before[3].split(']')[0]
    after = three_args[2].split(', ')
    after[0] = after[0].split('[')[1]
    after[3] = after[3].split(']')[0]
    instruction = three_args[1].split(' ')

#    print 'Before'
#    print before
#    print 'Instruction'
#    print instruction
#    print 'After'
#    print after
#    print '@'

    before_stack.append(before)
    instruction_stack.append(instruction)
    after_stack.append(after)
    


#Each instruction has four values:

# OPCODE INPUT_A INPUT_B OUTPUT_C
# OPCODE -- behavior
# value A == immediate A == literally A
# register A -- use register number
# C -- Always a register

#Four Registers:
#[0]
#[1]
#[2]
#[3]

def set_registers(zero, one, two, three):
    global registers
    registers[0] = int(zero)
    registers[1] = int(one)
    registers[2] = int(two)
    registers[3] = int(three)


def test_opcode(code_id, inputA, inputB, outputC):
    global registers
    
    #BORI
    if code_id == 0:
        #print 'Testing opcode bitwise OR immediate'
        registers[outputC] = registers[inputA] | int(inputB)


    #BORR
    elif code_id == 1:
        #print 'Testing opcode bitwise OR register'
        registers[int(outputC)] = registers[(inputA)] | registers[(inputB)]

    #SETI
    elif code_id == 2:
        #print 'Testing opcode set immediate'
        registers[int(outputC)] = int(inputA)

    #MULR
    elif code_id == 3:
        #multiply register register
        registers[int(outputC)] = registers[int(inputA)] * registers[int(inputB)]

    #SETR
    elif code_id == 4:
         # #Solved
        #print 'Testing opcode set register'
        registers[int(outputC)] = registers[(inputA)]

    #ADDR
    elif code_id == 5:
        #print 'Testing opcode add register'
        #Add two registers together
        registers[int(outputC)] = registers[int(inputA)] + registers[int(inputB)]


    #GTIR
    elif code_id == 6:
         #print 'Testing opcode greater than immediate register'
        if inputA > registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0

    #EQIR
    elif code_id == 7:
    
        #print 'Testing opcode equality immediate register'
        if inputA == registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0

    #GTRI
    elif code_id == 8:
    

        #print 'Testing opcode greater than register immediate'
        if registers[inputA] > inputB:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
    
    
    #BANI
    elif code_id == 9:
        #print 'Testing opcode bitwise AND immediate'
        registers[outputC] = registers[inputA] & int(inputB)

    #MULI
    elif code_id == 10:
        #print 'Testing opcode multiply immediate'
        #Multiply register with literal value
        registers[outputC] = registers[inputA] * int(inputB)

    #BARR
    elif code_id == 12:
        #print 'Testing opcode bitwise AND register'
        registers[int(outputC)] = registers[(inputA)] & registers[(inputB)]

    #GTRR
    elif code_id == 11:
        #FIGURED OUT (I THINK)
        #print 'Testing opcode greater than register register'
        if registers[inputA] > registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
            
    #EQRI
    elif code_id == 13:
        #print 'Testing opcode equality register immediate'
        if registers[inputA] == inputB:
            registers[outputC] = 1
        else:
            registers[outputC] = 0
    
    #ADDI
    elif code_id == 14:
        #print 'Testing opcode add immediate'
        #Add register with literal value
        registers[outputC] = registers[inputA] + int(inputB)

    #EQRR
    elif code_id == 15:
        #print 'Testing opcode equality register register'
        if registers[inputA] == registers[inputB]:
            registers[outputC] = 1
        else:
            registers[outputC] = 0


def test_equality(a, b, c, d):
    global registers
    if registers[0] == int(a) and registers[1] == int(b) and registers[2] == int(c) and registers[3] == int(d):
        return True
    else:
        return False

print 'Test a few out:'

def get_instructions_with_opcode(code_id):
    global before_stack
    global instruction_stack
    global after_stack
    global registers
    
    temp_before_stack = []
    temp_instruction_stack = []
    temp_after_stack = []

    for i in xrange(0, len(instruction_stack)):

        #print 'Comparing: ' + str(int(instruction_stack[i][0])) + ' and ' + str(int(code_id)) + ' and getting: ' + str(instruction_stack[i][0] == code_id)
        #print type(code_id)
        #print type(int(instruction_stack[i][0]))
        #print code_id
        if int(instruction_stack[i][0]) == code_id:
          #  print '...?'
            temp_before_stack.append(before_stack[i])
            temp_instruction_stack.append(instruction_stack[i])
            temp_after_stack.append(after_stack[i])

    return temp_before_stack, temp_instruction_stack, temp_after_stack


master_count = 0


#print 'Temp instruction: '
#print temp_instruction
solved_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
for full_num in xrange(0, 16):
    temp_before, temp_instruction, temp_after = get_instructions_with_opcode(int(full_num))
    success_map = {}
    success_list = []
    for n in xrange(0, 16):
        success_map[n] = (0,0)
        
        
        
        for count in xrange(0, len(temp_instruction)):
            before_temp = temp_before[count]
            instruction_temp = temp_instruction[count]
            afterward_temp = temp_after[count]
            set_registers(before_temp[0], before_temp[1], before_temp[2], before_temp[3])
            #print 'Before: '
            #print_regsiters()
            #print 'Executing instruction: '
            #print instruction_temp
            success_map[n] = (success_map[n][0], success_map[n][1]+1)
            test_opcode(n, int(instruction_temp[1]), int(instruction_temp[2]), int(instruction_temp[3]))
            if test_equality(afterward_temp[0], afterward_temp[1], afterward_temp[2], afterward_temp[3]):
                success_map[n] = (success_map[n][0]+1, success_map[n][1])
        for entry in success_map:
            if success_map[entry][0] == success_map[entry][1] and success_map[entry][0]!= 0 and entry not in success_list and entry not in solved_list:
                success_list.append(entry)
    print 'Success Map for OPCODE: ' + str(full_num)
    #print success_map
    print success_list
    print '\n'

    #print 'Ticker count: ' + str(ticker)


#Idea for isolating functionality -- write a function to get all instructions which have the same code, and then run them successively through each possible slot. See what happens. they should only work perfectly for one set



print '\n\n\n'
print 'RUNNING TEST PROGRAM...'

filename = 'sample program.txt'
s = open(filename, 'r').read()

s = s.split('\n')

instructions_sample = []
registers = [0,0,0,0]
for entry in s:
    instructions_sample.append(entry.split(' '))

for inst in instructions_sample:
    test_opcode(int(inst[0]), int(inst[1]), int(inst[2]), int(inst[3]))

print_regsiters()
