#Advent of Code Day 11 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

print s

serial_number = int(s)
'''
Find the fuel cell's rack ID, which is its X coordinate plus 10.
Begin with a power level of the rack ID times the Y coordinate.
Increase the power level by the value of the grid serial number (your puzzle input).
Set the power level to itself multiplied by the rack ID.
Keep only the hundreds digit of the power level (so 12345 becomes 3; numbers with no hundreds digit become 0).
Subtract 5 from the power level.
'''

val_map = {}

serial_number = 7400

for row in xrange(1, 301):
    for col in xrange(1, 301):
        
        power_level = 0
        rack_id = col + 10
        
        power_level = rack_id * row
        power_level = power_level + serial_number
        power_level = power_level*rack_id
        if power_level < 100:
            power_level = 0
        else:
            power_level = (power_level/100)%10
        power_level = power_level - 5
        
        
        val_map[(col, row)] = power_level


def print_map():
    global val_map
    
    for row in xrange(1, 301):
        str_out = ''
        for col in xrange(1, 301):
            str_out = str_out + str(val_map[(col, row)])
        print str_out

def get_cell_power_level((a, b)):
    global val_map
    return val_map[(a, b)];

def get_three_by_three((a,b)):
    global val_map
    total_score = 0
    for row in xrange(0,3):
        for col in xrange(0,3):
            total_score = total_score + val_map[(a+col, b+row)]
    return total_score

def get_n_by_n((a,b), n):
    global val_map
    total_score = 0
    for row in xrange(0,n):
        for col in xrange(0,n):
            total_score = total_score + val_map[(a+col, b+row)]
    return total_score

max_coord = (1,1)
max_value = 0
max_size = 1
size = 3

for k in xrange(1, 301):
    for row in xrange(1, 301-k+1):
        for col in xrange(1, 301-k+1):
            val = get_n_by_n((col, row), k)
            if val > max_value:
                max_value = val
                max_coord = (col, row)
                max_size = k
    print 'Completed ' + str(k)
    print 'Max val:' + str(max_value) + ' and coord is: ' + str(max_coord) + ', ' + str(max_size)

print_map()
print max_size
print 'Max val:' + str(max_value) + ' and coord is: ' + str(max_coord) + ', ' + str(max_size)
