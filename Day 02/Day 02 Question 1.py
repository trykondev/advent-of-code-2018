#Advent of Code Day 2 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

two_count = 0
two_count_flag = False

three_count = 0
three_count_flag = False

for item in s:
  
  count_hash = {}
  
  for c in item:
    if c not in count_hash:
      count_hash[c] = 1
    else:
      count_hash[c] = count_hash[c] + 1
  for x in count_hash.values():
    if x == 2:
      two_count_flag = True
    elif x == 3:
      three_count_flag = True
  
  if two_count_flag:
    two_count = two_count + 1
    two_count_flag = False
  if three_count_flag:
    three_count = three_count + 1
    three_count_flag = False
  
print 'Two Count: ' + str(two_count)
print 'Three Count: ' + str(three_count)
print 'Checksum: ' + str(two_count * three_count)