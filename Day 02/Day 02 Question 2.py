#Advent of Code Day 2 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

final_answer_found = False

for item_a in s:
  for item_b in s:
    mismatch_count = 0
    mismatch_index = 0
    for i in xrange(0, len(item_a)):
      if item_a[i] != item_b[i]:
        mismatch_count = mismatch_count + 1
        mismatch_index = i
    if mismatch_count == 1:
      final_item = ''
      if (mismatch_index == (len(item_a) - 1)):
        final_item = item_a[0:len(item_a)-2]
      else:
        final_item = item_a[0:mismatch_index] + item_a[mismatch_index+1:]
      print 'First ID: ' + item_a
      print 'Second ID: ' + item_b
      print 'Final output: ' + final_item
      final_answer_found = True
    if final_answer_found:
      break
  if final_answer_found:
      break
