"""
Matplotlib Animation Example

author: Jake Vanderplas
email: vanderplas@astro.washington.edu
website: http://jakevdp.github.com
license: BSD
Please feel free to use and modify this, but keep the above information. Thanks!
"""



#Advent of Code Day 10 Question 1

import sys
import os
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

filename = 'input.txt'
s = open(filename, 'r').read()

s = '''position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>'''

s = open(filename, 'r').read()
s = s.split('\n')

for entry in s:
    print entry

time_step = 0
coord_map = {}

min_x = 0
min_y = 0

x_list = []
y_list = []

for entry in s:
    entry = entry.split('=')
    first_coord = entry[1].split('<')
    first_coord = first_coord[1]
    first_coord = first_coord.split('>')[0]
    first_coord = first_coord.split(',')
    second_coord = entry[2].split('<')
    second_coord = second_coord[1]
    second_coord = second_coord.split('>')[0]
    second_coord = second_coord.split(',')
    
    first_coord = (int(first_coord[0]), int(first_coord[1]))
    second_coord = (int(second_coord[0]), int(second_coord[1]))

    if(first_coord[0] < min_x):
        min_x = first_coord[0]
    if(first_coord[1] < min_y):
        min_y = first_coord[0]


ultimate_second_counter = 10600

label = 'A'
for entry in s:
    entry = entry.split('=')
    first_coord = entry[1].split('<')
    first_coord = first_coord[1]
    first_coord = first_coord.split('>')[0]
    first_coord = first_coord.split(',')
    second_coord = entry[2].split('<')
    second_coord = second_coord[1]
    second_coord = second_coord.split('>')[0]
    second_coord = second_coord.split(',')
    
    first_coord = (int(first_coord[0])-min_x, int(first_coord[1])-min_y, label)
    second_coord = (int(second_coord[0]), int(second_coord[1]))
    coord_map[first_coord] = second_coord
    if ord(label) == 255:
        label = chr(0)
    label = chr(ord(label)+1)

print coord_map

def update_points():
    global time_step
    global coord_map
    next_map = {}
    for entry in coord_map:
        velocities = coord_map[entry]
        entry = (entry[0] + velocities[0], entry[1] + velocities[1], entry[2])
        next_map[entry] = velocities
    coord_map = next_map


def prepare_points():
    global x_list
    global y_list
    list_of_coords = []
    final_str = ''
    x_list = []
    y_list = []
    for entry in coord_map:
        list_of_coords.append((entry[0], entry[1]))
        x_list.append(entry[0])
        y_list.append(entry[1])

    update_points()

print 'x and y size: ' + str(len(x_list)) + ', ' + str(len(y_list))



for m in xrange(0, 10600):
    prepare_points()

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 10), ylim=(-2, 10))
ax.set_facecolor((0, 0, 0))
line, = ax.plot([], [], lw=2)
plt.rcParams['axes.facecolor'] = 'black'
fig = plt.figure()
fig.patch.set_facecolor('black')
current_scatter = 0
# initialization function: plot the background of each frame
def init():
    global x_list;
    global y_list;
    global current_scatter;
    # Fixing random state for reproducibility
    np.random.seed(19680801)


    N = 50

    prepare_points()


    current_scatter = plt.scatter(x_list, y_list, color='yellow')
    #line.set_data(x, y)
    return current_scatter,

# animation function.  This is called sequentially
def animate(i):
    global x_list
    global y_list
    global ultimate_second_counter
    global current_scatter
    for count in xrange(0, len(x_list)):
        x_list[count]
    for count in xrange(0, len(y_list)):
        y_list[count]

    current_scatter.remove()
    prepare_points()
    current_scatter = plt.scatter(x_list, y_list, color='yellow')
    ultimate_second_counter = ultimate_second_counter + 1
    print 'Second count: ' + str(ultimate_second_counter)
    return current_scatter,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=2000000, interval=1650, blit=False)


plt.clf()
plt.show()
