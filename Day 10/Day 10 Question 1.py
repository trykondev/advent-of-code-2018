#Advent of Code Day 10 Question 1

import sys
import os
import numpy as np
import matplotlib.pyplot as plt

filename = 'input.txt'
s = open(filename, 'r').read()

s = '''position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>'''

#s = open(filename, 'r').read()
s = s.split('\n')

for entry in s:
    print entry

time_step = 0
coord_map = {}


min_x = 0
min_y = 0

for entry in s:
    entry = entry.split('=')
    first_coord = entry[1].split('<')
    first_coord = first_coord[1]
    first_coord = first_coord.split('>')[0]
    first_coord = first_coord.split(',')
    second_coord = entry[2].split('<')
    second_coord = second_coord[1]
    second_coord = second_coord.split('>')[0]
    second_coord = second_coord.split(',')
    
    first_coord = (int(first_coord[0]), int(first_coord[1]))
    second_coord = (int(second_coord[0]), int(second_coord[1]))

    if(first_coord[0] < min_x):
        min_x = first_coord[0]
    if(first_coord[1] < min_y):
        min_y = first_coord[0]





label = 'A'
for entry in s:
    entry = entry.split('=')
    first_coord = entry[1].split('<')
    first_coord = first_coord[1]
    first_coord = first_coord.split('>')[0]
    first_coord = first_coord.split(',')
    second_coord = entry[2].split('<')
    second_coord = second_coord[1]
    second_coord = second_coord.split('>')[0]
    second_coord = second_coord.split(',')
    
    first_coord = (int(first_coord[0])-min_x, int(first_coord[1])-min_y, label)
    second_coord = (int(second_coord[0]), int(second_coord[1]))
    coord_map[first_coord] = second_coord
    if ord(label) == 255:
        label = chr(0)
    label = chr(ord(label)+1)

print coord_map

def update_points():
    global time_step
    global coord_map
    next_map = {}
    for entry in coord_map:
        velocities = coord_map[entry]
        entry = (entry[0] + velocities[0], entry[1] + velocities[1], entry[2])
        next_map[entry] = velocities
    coord_map = next_map


for time_limit in xrange(0, 600):

    list_of_coords = []
    final_str = ''
    x_list = []
    y_list = []
    for entry in coord_map:
        list_of_coords.append((entry[0], entry[1]))
        x_list.append(entry[0])
        y_list.append(entry[1])

    if time_limit == 0:
        plt.plot(x_list, y_list, 'ro')
        #plt.axis([-80000, 500000, -50000, 150000])
        plt.axis([-80, 50, -50, 150])
        plt.show()

    else:
        plt.clear()
        plt.figure().set_ydata(y_list)
        plt.figure().set_xdata(x_list)
    update_points()

print 'x and y size: ' + str(len(x_list)) + ', ' + str(len(y_list))

    
    '''
    
    
fig = plt.figure()
ax = fig.add_subplot(111)
line1, = ax.plot(x, y, 'r-') # Returns a tuple of line objects, thus the comma

for phase in np.linspace(0, 10*np.pi, 500):
    line1.set_ydata(np.sin(x + phase))
    fig.canvas.draw()
    fig.canvas.flush_events()
    
    
    
    
    for row in xrange(0, 700):
        str_temp = ''
        for col in xrange(0,700):
            if (col, row) in list_of_coords:
                #str_temp = str_temp + '#'
                x_list.append(col)
                y_list.append(row)
            #else:
                #str_temp = str_temp + '.'
        #final_str = final_str + str_temp + '\n'
    
    pop_count = 0
    for e in final_str:
        if e == '#':
            pop_count = pop_count + 1
    if pop_count > 5:
        print final_str
    update_points()
    print ''
    '''

print 'x and y size: ' + str(len(x_list)) + ', ' + str(len(y_list))





