#Advent of Code Day 15 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

max_cols = 0
max_rows = 0
max_width = 0
for entry in s:
    #print entry
    if len(entry) > max_width:
        max_width = len(entry)

print 'coords: ' + str(len(s)) + ' ' + str(max_width)
grid = [[' ' for x in xrange(max_width + 1)] for y in xrange(0, len(s))]
max_cols = max_width + 1
max_rows = len(s)
num = 0


goblin_map = {}
elf_map = {}

for row in xrange(0, len(s)):
    for col in xrange(0, len(s[row])):
        #print 'row, col: ' + str(row) + ', ' + str(col)
        grid[row][col] = s[row][col]
        if grid[row][col] == 'G':
            print 'found goblin'
            goblin_map[(row, col)] = (200)
        elif grid[row][col] == 'E':
            print 'found elf'
            elf_map[(row, col)] = (200)




def print_grid():
    global grid
    global max_width
    global cart_map
    for row in xrange(0, len(s)):
        str_out = ''
        for col in xrange(0, max_width):
            str_out = str_out + str(grid[row][col])
        print str_out


print_grid()
print 'Goblin map:'
print goblin_map
print 'Elf map:'
print elf_map

def goblin_turn():
    print 'Goblin turn!'


def elf_turn():
    print 'Elf turn!'


rounds = 4

for i in xrange(0, rounds):
    print '-----------------'
    print 'ROUND ' + str(i+1)
    print '-----------------'

    #In reading order, go through a list of units, and for ecah one, do their turn
    
    
    print '\n'
    #This is spacing for the end of the loop

print 'FINISHED THE GAME!!'
