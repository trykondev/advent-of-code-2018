#Advent of Code Day 8 Question 2

import sys
import os

class Node(object):
    metadata_values = []
    q2_value = 0
    children = []
    parent = None
    label = None

    def __init__(self, name):
        self.label = name;
        self.children = []
        self.parent = None
        self.metadata_values = []
    def __str__(self):
        if self.label == None:
            self.label = 'notset'
        temp = 'My name is ' + self.label + '. '
        temp = temp + 'I have ' + str(len(self.children)) + ' children. '
        temp = temp + 'I have ' + str(len(self.metadata_values)) + ' metadata values. '
        for n in xrange (0, len(self.metadata_values)):
            temp = temp + ' ' + str(self.metadata_values[n])
        return temp

    def compute_value(self):
        self.q2_value = 0
        if len(self.children) == 0:
            for m in xrange(0, len(self.metadata_values)):
                self.q2_value = self.q2_value + int(self.metadata_values[m])
        else:
            valid_index_list = []
            for m in xrange(0, len(self.children)):
                valid_index_list.append(m)
            print 'valid index list: '
            print valid_index_list
            for m in xrange(0, len(self.metadata_values)):
                if self.metadata_values[m]-1 in valid_index_list:
                    self.q2_value = self.q2_value + self.children[self.metadata_values[m]-1].compute_value()
        return self.q2_value

filename = 'input.txt'
s = open(filename, 'r').read()

#s = '2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2'
s = s.split(' ')


total_list = []
for element in s:
    print element
    total_list.append(element)
total_list.reverse()

tree = {}

total_val = 0

'''
while(len(total_list) > 0):
    child_quant = total_list.pop()
    meta_quant = total_list.pop()
'''
#coord_label = chr(ord(coord_label)+1)
label = 'A'

'''
root = Node(label)

root.label = label
label = chr(ord(label)+1)
root.parent = None
'''

root = None

def process_list(t_list, c_quant, m_quant, parent):
    global total_val, label
    if c_quant == 0:
        print 'base case'
        next = Node(label)
        print 'created node ' + label
        if(ord(label) + 1 > 255):
            label = chr(0)
        label = chr(ord(label)+1)
        next.parent = parent
        parent.children.append(next)
        for i in xrange(0, m_quant):
            extra_val = int(t_list.pop())
            print 'adding: ' + str(extra_val)
            next.metadata_values.append(extra_val)
    else:
        next = Node(label)
        
        if(ord(label) + 1 > 255):
            label = chr(0)
        label = chr(ord(label)+1)
        if parent != None:
            next.parent = parent
            parent.children.append(next)
        for j in xrange(0, c_quant):
            print 'recurse!'
            process_list(t_list, int(t_list.pop()), int(t_list.pop()), next)


        for k in xrange(0, m_quant):
            extra_val = int(t_list.pop())
            print 'adding: ' + str(extra_val)
            next.metadata_values.append(extra_val)
    return next

root = process_list(total_list, int(total_list.pop()), int(total_list.pop()), root)

queue = []
queue.append(root)

root.compute_value()
print 'Computing value! It is: ' + str(root.compute_value())
while len(queue) > 0:
    next = queue.pop()
    print next
    next.compute_value()
    for x in xrange(len(next.children)):
        queue.append(next.children[x])

print '\n'

print 'This is my root: '
print root
print root.compute_value()
