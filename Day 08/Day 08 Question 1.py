#Advent of Code Day 8 Question 1

import sys
import os

class Node:
    metadata_value = 0
    children = []

filename = 'input.txt'
s = open(filename, 'r').read()

#s = '2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2'
s = s.split(' ')


total_list = []
for element in s:
    print element
    total_list.append(element)
total_list.reverse()

tree = {}

total_val = 0

'''
while(len(total_list) > 0):
    child_quant = total_list.pop()
    meta_quant = total_list.pop()
'''

def process_list(t_list, c_quant, m_quant):
    global total_val
    if c_quant == 0:
        print 'base case'

        for i in xrange(0, m_quant):
            extra_val = int(t_list.pop())
            print 'adding: ' + str(extra_val)
            total_val = total_val + extra_val
    else:
        for j in xrange(0, c_quant):
            print 'recurse!'
            process_list(t_list, int(t_list.pop()), int(t_list.pop()))

        for k in xrange(0, m_quant):
            extra_val = int(t_list.pop())
            print 'adding: ' + str(extra_val)
            total_val = total_val + extra_val

process_list(total_list, int(total_list.pop()), int(total_list.pop()))
print 'Finally! All done'
print 'Final result: ' + str(total_val)
