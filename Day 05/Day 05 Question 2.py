#Advent of Code Day 5 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()


s = open(filename, 'r').read()

def get_polymer_length(input_str):
  s = input_str
  #print 'First polymer: ' + s
  completely_finished = False
  count = 0
  while not completely_finished:
    flag = True
    new_s = ''
    i = 0
    while(i < len(s)):
      count = count + 1
      if i + 1 == len(s):
        new_s = new_s + s[i]
        break
      if((ord(s[i]) + 32 == ord(s[i+1])) or (ord(s[i]) - 32 == ord(s[i+1]))):
        i = i + 2
        flag = False
      else:
        new_s = new_s + s[i]
        i=i+1

    completely_finished = flag
    #print 'Current polymer: ' + new_s
    #print 'Flag is: ' + str(flag)
    s = new_s

  return len(s)
    
all_polymers = {}

for i in range(65, 65+26):
  #print chr(i)
  next_polymer = ''
  for char in s:
    if ord(char) == i or ord(char) == (i+32):
      continue
    else:
      next_polymer = next_polymer + char
  all_polymers[i] = next_polymer
  
for key in all_polymers:
  print 'Len is: ' + str(get_polymer_length(all_polymers[key]))

#Manually verify which of these values is the smallest
    
  
  
  