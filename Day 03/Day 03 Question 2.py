#Advent of Code Day 3 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()
s_arr = s.split('\n')

fabric_hash = {}


for item in s_arr:
  vals = item.split(' ')
  id = vals[0][1:]
  coords = vals[2][:len(vals[2])-1]
  dimensions = vals[3]
  
  coords_array = coords.split(',')
  coord_col = int(coords_array[0])
  coord_row = int(coords_array[1])
  
  dims_array = dimensions.split('x')
  dim_width = int(dims_array[0])
  dim_height = int(dims_array[1])

  max_i = 0
  max_j = 0
  for i in xrange(coord_row, coord_row + dim_height):
    for j in xrange(coord_col, coord_col + dim_width):
      if (i,j) in fabric_hash:
        fabric_hash[(i,j)] = 'X'
      else:
        fabric_hash[(i,j)] = '#'
      if j > max_j:
        max_j = j
      if i > max_i:
        max_i = i
  
  
  
for item in s_arr:
  vals = item.split(' ')
  id = vals[0][1:]
  coords = vals[2][:len(vals[2])-1]
  dimensions = vals[3]
  
  coords_array = coords.split(',')
  coord_col = int(coords_array[0])
  coord_row = int(coords_array[1])
  
  dims_array = dimensions.split('x')
  dim_width = int(dims_array[0])
  dim_height = int(dims_array[1])

  max_i = 0
  max_j = 0
  winner = True
  
  for i in xrange(coord_row, coord_row + dim_height):
    for j in xrange(coord_col, coord_col + dim_width):
      if fabric_hash[(i,j)] == 'X':
        winner = False
  if winner:
    print 'Non-overlapping claim: #' + id
    break
