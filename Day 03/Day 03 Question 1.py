#Advent of Code Day 3 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()
s_arr = s.split('\n')

fabric_hash = {}

max_i = 0
max_j = 0

for item in s_arr:
  vals = item.split(' ')
  id = vals[0][1:]
  coords = vals[2][:len(vals[2])-1]
  dimensions = vals[3]
  
  coords_array = coords.split(',')
  coord_col = int(coords_array[0])
  coord_row = int(coords_array[1])
  
  dims_array = dimensions.split('x')
  dim_width = int(dims_array[0])
  dim_height = int(dims_array[1])


  for i in xrange(coord_row, coord_row + dim_height):
    for j in xrange(coord_col, coord_col + dim_width):
      if (i,j) in fabric_hash:
        fabric_hash[(i,j)] = 'X'
      else:
        fabric_hash[(i,j)] = '#'
      if j > max_j:
        max_j = j
      if i > max_i:
        max_i = i

final_x_count = 0

for i in xrange(0, max_i+2):
  str_var = ''
  for j in xrange(0, max_j+2):
    if (i,j) in fabric_hash:
      value = str(fabric_hash[(i,j)])
      if value == '#':
        str_var = str_var + value
      else:
        str_var = str_var + 'X'
      if(value == 'X'):
        final_x_count = final_x_count + 1

    else:
      str_var = str_var + "."
  #print str_var

print 'Final overlap count: ' + str(final_x_count)
  


  
