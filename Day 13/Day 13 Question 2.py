#Advent of Code Day 13 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()


s="""/->-\\
|   |  /----\\
| /-+--+-\  |
| | |  | v  |
\-+-/  \-+--/
  \------/"""

s="""|
v
|
|
|
|
^
|
|"""

s="""/>-<\\
|   |
| /<+-\\
| | | v
\>+</ |
  |   ^
  \<->/"""

s = open(filename, 'r').read()
s = s.split('\n')



max_cols = 0
max_rows = 0
max_width = 0
for entry in s:
    #print entry
    if len(entry) > max_width:
        max_width = len(entry)

print 'coords: ' + str(len(s)) + ' ' + str(max_width)
grid = [[' ' for x in xrange(max_width + 1)] for y in xrange(0, len(s))]
max_cols = max_width + 1
max_rows = len(s)
num = 0
for row in xrange(0, len(s)):
    for col in xrange(0, len(s[row])):
        #print 'row, col: ' + str(row) + ', ' + str(col)
        grid[row][col] = s[row][col]
cart_map = {}

def print_grid():
    global grid
    global max_width
    global cart_map
    for row in xrange(0, len(s)):
        str_out = ''
        for col in xrange(0, max_width):
            if (row, col) in cart_map:
                str_out = str_out + str(cart_map[(row,col)][0])
            else:
                str_out = str_out + str(grid[row][col])
        print str_out

print_grid()
collision_detected = False
collision_location = (0,0)
#What will I need to know for each cart?


def detect_cart(row, col):
    global grid
    if grid[row][col] == 'v' or grid[row][col] == '>' or grid[row][col] == '^' or grid[row][col] == '<':
        return True
    return False

#def detect_single_cart():


def move_cart((row, col), (symbol, c_id, last_turn)):
    global grid
    global cart_map
    global collision_detected
    global collision_location
    print 'moving: ' + symbol
    col_this_time = False
    if symbol == '>':
        if (row, col+1) in cart_map:
            col_this_time = True
            print 'COLLIDED BUD!'
            collision_detected = True
            print 'collided at X,Y of ' + str(col+1) + ', ' + str(row)
            collision_location = (col+1, row)
            del cart_map[(row, col)]
            cart_keys = cart_map.keys()
            for entry in cart_keys:
                print 'entry and loc'
                print entry
                print collision_location
                if entry == (row, col+1):
                    print 'deletin'
                    del cart_map[entry]
                if entry == (row, col):
                    print 'deletin'
                    del cart_map[(row, col)]
        elif grid[row][col+1] == '-':
            cart_map[(row, col+1)] = (symbol, c_id, last_turn)
        elif grid[row][col+1] == '\\':
            cart_map[(row, col+1)] = ('v', c_id, last_turn)
        elif grid[row][col+1] == '/':
            cart_map[(row, col+1)] = ('^', c_id, last_turn)
        elif grid[row][col+1] == '+':
            next_sym = turn_cart(symbol, last_turn)
            if last_turn == -1:
                last_turn = 0
            elif last_turn == 0:
                last_turn = 1
            elif last_turn == 1:
                last_turn = -1
            cart_map[(row, col+1)] = (next_sym, c_id, last_turn)
    elif symbol == 'v':
        if (row+1, col) in cart_map:
            col_this_time = True
            print 'COLLIDED BUD!'
            collision_detected = True
            print 'collided at X,Y of ' + str(col) + ', ' + str(row+1)
            collision_location = (col, row+1)
            del cart_map[(row, col)]
            cart_keys = cart_map.keys()
            for entry in cart_keys:
                print 'entry and loc'
                print entry
                print collision_location
                if entry == (row+1, col):
                    print 'deletin'
                    del cart_map[entry]
                if entry == (row, col):
                    print 'deletin'
                    del cart_map[(row, col)]
        elif grid[row+1][col] == '|':
            print 'found this thing tho'
            cart_map[(row+1, col)] = (symbol, c_id, last_turn)
        elif grid[row+1][col] == '\\':
            cart_map[(row+1, col)] = ('>', c_id, last_turn)
        elif grid[row+1][col] == '/':
            cart_map[(row+1, col)] = ('<', c_id, last_turn)
        elif grid[row+1][col] == '+':
            next_sym = turn_cart(symbol, last_turn)
            if last_turn == -1:
                last_turn = 0
            elif last_turn == 0:
                last_turn = 1
            elif last_turn == 1:
                last_turn = -1
            cart_map[(row+1, col)] = (next_sym, c_id, last_turn)
    elif symbol == '<':
        if (row, col-1) in cart_map:
            col_this_time = True
            print 'COLLIDED BUD!'
            collision_detected = True
            print 'collided at X,Y of ' + str(col-1) + ', ' + str(row)
            collision_location = (col-1, row)
            #del cart_map[(col, row)]
            cart_keys = cart_map.keys()
            for entry in cart_keys:
                print 'entry and loc'
                print entry
                print collision_location
                if entry == (row, col-1):
                    print 'deletin'
                    del cart_map[entry]
                if entry == (row, col):
                    print 'deletin'
                    del cart_map[(row, col)]
        elif grid[row][col-1] == '-':
            cart_map[(row, col-1)] = (symbol, c_id, last_turn)
        elif grid[row][col-1] == '\\':
            cart_map[(row, col-1)] = ('^', c_id, last_turn)
        elif grid[row][col-1] == '/':
            cart_map[(row, col-1)] = ('v', c_id, last_turn)
        elif grid[row][col-1] == '+':
            next_sym = turn_cart(symbol, last_turn)
            if last_turn == -1:
                last_turn = 0
            elif last_turn == 0:
                last_turn = 1
            elif last_turn == 1:
                last_turn = -1
            cart_map[(row, col-1)] = (next_sym, c_id, last_turn)
    elif symbol == '^':
        if (row-1, col) in cart_map:
            col_this_time = True
            print 'COLLIDED BUD!'
            collision_detected = True
            print 'collided at X,Y of ' + str(col) + ', ' + str(row-1)
            collision_location = (col, row-1)
            del cart_map[(row, col)]
            cart_keys = cart_map.keys()
            for entry in cart_keys:
                print 'entry and loc'
                print entry
                print collision_location
                if entry == (row-1, col):
                    print 'deletin'
                    del cart_map[entry]
                if entry == (row, col):
                    print 'deletin'
                    del cart_map[(row, col)]
        elif grid[row-1][col] == '|':
            cart_map[(row-1, col)] = (symbol, c_id, last_turn)
        elif grid[row-1][col] == '\\':
            cart_map[(row-1, col)] = ('<', c_id, last_turn)
        elif grid[row-1][col] == '/':
            cart_map[(row-1, col)] = ('>', c_id, last_turn)
        elif grid[row-1][col] == '+':
            next_sym = turn_cart(symbol, last_turn)
            if last_turn == -1:
                last_turn = 0
            elif last_turn == 0:
                last_turn = 1
            elif last_turn == 1:
                last_turn = -1
            cart_map[(row-1, col)] = (next_sym, c_id, last_turn)
    if not col_this_time:
        del cart_map[(row, col)]
def replace_cart_with_track(cart_symbol):
    if cart_symbol == '>':
        return '-'
    elif cart_symbol == '^':
        return '|'
    elif cart_symbol == '<':
        return '-'
    elif cart_symbol == 'v':
        return '|'

def turn_cart(cart_symbol, direction):
    #Turning LEFT
    if direction == -1:
        if cart_symbol == '>':
            return '^'
        elif cart_symbol == '^':
            return '<'
        elif cart_symbol == '<':
            return 'v'
        elif cart_symbol == 'v':
            return '>'
    #Going STRAIGHT
    elif direction == 0:
        if cart_symbol == '>':
            return '>'
        elif cart_symbol == '^':
            return '^'
        elif cart_symbol == '<':
            return '<'
        elif cart_symbol == 'v':
            return 'v'

    #Turn RIGHT
    elif direction == 1:
        if cart_symbol == '>':
            return 'v'
        elif cart_symbol == '^':
            return '>'
        elif cart_symbol == '<':
            return '^'
        elif cart_symbol == 'v':
            return '<'


#Begin Algorithm

cart_id = 0
last_turn_temp = -1
print '\n\n\nBegin Algorithm\n'
#Figure out where all the carts are
for row in xrange(max_rows):
    for col in xrange(max_cols):
        if detect_cart(row, col):
            cart_map[(row, col)] = (grid[row][col], cart_id, last_turn_temp)
            print 'Found a cart!'

#Now replace those carts in the map with no carts
for entry in cart_map:
    grid[entry[0]][entry[1]] = replace_cart_with_track(cart_map[entry][0])

print 'printing grid no carts: '

tick = 0
for step in xrange(0, 300000):
    print cart_map
    cart_order = sorted(cart_map.keys())
    if len(cart_order) == 1:
        print 'FOUND ER BUD!'
        print 'The final surviving cart is at: ' + str(cart_order[0][1]) + ', ' + str(cart_order[0][0])
        break
    for cart in cart_order:
        print cart
        if cart in cart_map:
            print cart_map[cart]

            move_cart(cart, cart_map[cart])
       # print_grid()
        print '@@@@'
    tick = tick + 1


