#Advent of Code Day 6 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

'''
s = 1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
'''


s = s.split('\n')

def manhattan_distance(x_start, y_start, x_target, y_target):
  return abs(x_target - x_start) + abs(y_target - y_start)

max_x = 0
max_y = 0

coord_label = 'A'
coord_map = {}
overall_map = {}

for entry in s:
  arr = entry.split(',')
  x = int(arr[0])
  y = int(arr[1])
  
  if x > max_x:
    max_x = x
    
  if y > max_y:
    max_y = y
    
  coord_map[(x,y)]= coord_label
  coord_label = chr(ord(coord_label)+1)



#Populate map with blank values
for j in xrange(0, max_y+2):
  str_temp = ''
  for i in xrange(0, max_x+2):
    if (i,j) not in coord_map:
      overall_map[(i,j)] = '.'
    else:
      overall_map[(i,j)] = coord_map[(i,j)]


for j in xrange(0, max_y+2):
  for i in xrange(0, max_x+2):
    min_distance = 99999999
    all_points_map = {}
    for coord in coord_map:
      val = manhattan_distance(i, j, coord[0], coord[1])
      ch = coord_map[coord]
      all_points_map[(val, ch)] = True
      
      
  # print 'Sorted keys: ' 
    sorted_vals = sorted(all_points_map.keys())
    overall_map[(i,j)] = sorted_vals[0][1]
    min_number = sorted_vals[0][0]
    #print 'min number is: ' + str(min_number)
    for z in xrange(1, len(sorted_vals)):
      #print sorted_vals[z]
      if sorted_vals[z][0] == min_number:
        overall_map[(i,j)] = '.'
        break
  


for j in xrange(0, max_y+2):
  str_temp = ''
  for i in xrange(0, max_x+2):
    str_temp = str_temp + overall_map[(i,j)]
      
  
infinite_values = []

#Build list of infinite grids
#Realized by submitting incorrect answer this DOES NOT WORK -- only checks outside perimeter, but infinite
#nodes may not be on outer edge. Manually sifted through results to get the gold star. 
#TODO: Fix this to correctly detect all infinite nodes
for j in xrange(0, max_y+2):
  for i in xrange(0, max_x+2):
    if i == 0:
      infinite_values.append(overall_map[(i,j)])
    if j == 0:
      infinite_values.append(overall_map[(i,j)])
    if i == max_y + 1:
      infinite_values.append(overall_map[(i,j)])
    if j == max_x + 1:
      infinite_values.append(overall_map[(i,j)])


infinite_values = set(infinite_values)

parse_set = set(overall_map.values())

count_map = {}
for entry in parse_set:
  for val in overall_map.values():
    if val == entry and entry not in infinite_values:
      if entry in count_map:
        tmp = count_map[entry][0]
        tmp = tmp + 1
        count_map[entry] = (tmp, entry)
      else:
        count_map[entry] = (1, entry)
      
print sorted(count_map.values(), reverse = True)

  
