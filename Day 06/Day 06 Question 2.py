#Advent of Code Day 6 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

'''
s = 1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
'''


s = s.split('\n')

def manhattan_distance(x_start, y_start, x_target, y_target):
  return abs(x_target - x_start) + abs(y_target - y_start)
  

max_x = 0
max_y = 0

coord_label = 'A'
coord_map = {}
overall_map = {}

for entry in s:
  arr = entry.split(',')
  x = int(arr[0])
  y = int(arr[1])
  
  if x > max_x:
    max_x = x
    
  if y > max_y:
    max_y = y
    
  coord_map[(x,y)]= coord_label
  coord_label = chr(ord(coord_label)+1)



#Populate map with blank values
for j in xrange(0, max_y+2):
  str_temp = ''
  for i in xrange(0, max_x+2):
    if (i,j) not in coord_map:
      overall_map[(i,j)] = '.'
    else:
      overall_map[(i,j)] = coord_map[(i,j)]
      

for j in xrange(0, max_y+2):
  for i in xrange(0, max_x+2):
    min_distance = 99999999
    all_points_map = {}
    distance_sum = 0
    for coord in coord_map:
      val = manhattan_distance(i, j, coord[0], coord[1])
      ch = coord_map[coord]
      all_points_map[(val, ch)] = True
      distance_sum += val
      
    if(distance_sum < 10000):
      overall_map[(i,j)] = '#'

      
for j in xrange(0, max_y+2):
  str_temp = ''
  for i in xrange(0, max_x+2):
    str_temp = str_temp + overall_map[(i,j)]
      

parse_set = set(overall_map.values())

count_map = {}
for entry in parse_set:
  for val in overall_map.values():
    if val == entry:
      if entry in count_map:
        tmp = count_map[entry][0]
        tmp = tmp + 1
        count_map[entry] = (tmp, entry)
      else:
        count_map[entry] = (1, entry)
      
res = sorted(count_map.values(), reverse = True)
result_count = 0

for e in res:
  if e[1] == '#':
    result_count = e[0]
    break

print 'Total number of locations in region: ' + str(result_count)
  
