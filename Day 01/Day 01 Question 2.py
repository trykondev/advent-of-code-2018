#Advent of Code Day 1 Question 2

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

total = 0
freq = {}

freq[0] = True
found_it = False
result = 0
while(not found_it):
  for item in s:
    if(item[0] == '+'):
      total += int(item[1:])
      if(total in freq):
        result = total
        found_it = True
        break
      else:
        freq[total] = True
    else:
      total -= int(item[1:])
      if(total in freq):
        result = total
        found_it = True
        break
      else:
        freq[total] = True

print result