#Advent of Code Day 1 Question 1

import sys
import os

filename = 'input.txt'
s = open(filename, 'r').read()

s = s.split('\n')

total = 0

for item in s:
  if(item[0] == '+'):
    total += int(item[1:])
  else:
    total -= int(item[1:])

print total